= A Plan for Connect 4 =

* Game board
    * Grid 7x6
    * Starts empty
* Pieces: red and black
* Winning message
* Allowing the playes to make a move
    * Whose turn it is
    * What columns area available to move in 
       * clicking on a full column: Show a message
* Endgame conditions
    * Checking to see if someone has won
        * horizonally
        * vertically
        * diagonally
    * Checking for a tie
        * All of the cells are full, and there is no winner

== Planning the implementation ==

* Initializaion
    * Display instructions
    * Identify the players
    * Identify the current player
    * Board
        * data model - how the board is represented in the JS code
            *  two-dimensional array
            {
                  ['', '', '', '', '', '', ''],
                  ['', '', '', '', '', '', ''],
                  ['', '', '', '', '', '', ''],
                  ['', '', '', '', '', '', ''],
                  ['', '', '', '', '', '', ''],
                  ['', '', '', '', '', '', ''],
            }
            * States of a cell;
                1. Empty              '' (Empty string)
                2. Player 1 (red)   1
                3. PLayer 2 (black) 2
        * presentation - how to use the data model to display the board to the user
            * <table>
            * nested <div> elements rendered using flexbox
    * Set up the controls ()
        * Clickevent handlers attached to columns using delegation
* Reacting to user input when the user clicks on a column
    * drop a disk into that column
        * Figure out which cell in the column the disk will stop on
    * Check for ending condition
        * tie
            * Entire board is full
        * win
            * horizonally
            * vertically
            * diagonally
    * Switch the next player
* End game stuff
    * show a message describing the outcome of the game
    * play again or end the game
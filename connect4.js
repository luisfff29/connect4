
const td = document.querySelectorAll("td");
const chip = document.querySelector(".chip");
const table = document.querySelector(".table");

for(let i = 0; i < td.length; i++) {
  td[i].addEventListener("click", goDown)
}

function goDown() {
  let copiedChip = document.createElement("div");
  copiedChip.classList.add("copiedChip");
  
  checkingBoard();
  showChip(this);
  
  changeColor();
  
  function showChip(square) {
    let column = square.className;
    
    let refill = [];
    
    for (let i = 0; i < td.length; i++) {
      if(td[i].classList.contains(column)) {
        refill.push(td[i]);
      }
    }
    
    for (let i = 0; i < refill.length; i++) {
      if (refill[i].hasChildNodes()) {
        return;
      } else {
        refill[i].appendChild(copiedChip);
      }
    }
  }

  function changeColor() {
    if (chip.classList.contains("black")) {
      chip.classList.remove("black");
      copiedChip.classList.add("black");
    } else {
      chip.classList.add("black");
      copiedChip.classList.remove("black");
    }
  } 
}


function checkingBoard() {

const winningCombinations = [
  ["1x1", "1x2", "1x3", "1x4"],
  ["1x2", "1x3", "1x4", "1x5"],
  ["1x3", "1x4", "1x5", "1x6"],
  ["1x4", "1x5", "1x6", "1x7"],
  ["2x1", "2x2", "2x3", "2x4"],
  ["2x2", "2x3", "2x4", "2x5"],
  ["2x3", "2x4", "2x5", "2x6"],
  ["2x4", "2x5", "2x6", "2x7"],
  ["3x1", "3x2", "3x3", "3x4"],
  ["3x2", "3x3", "3x4", "3x5"],
  ["3x3", "3x4", "3x5", "3x6"],
  ["3x4", "3x5", "3x6", "3x7"],
  ["4x1", "4x2", "4x3", "4x4"],
  ["4x2", "4x3", "4x4", "4x5"],
  ["4x3", "4x4", "4x5", "4x6"],
  ["4x4", "4x5", "4x6", "4x7"],
  ["5x1", "5x2", "5x3", "5x4"],
  ["5x2", "5x3", "5x4", "5x5"],
  ["5x3", "5x4", "5x5", "5x6"],
  ["5x4", "5x5", "5x6", "5x7"],
  ["6x1", "6x2", "6x3", "6x4"],
  ["6x2", "6x3", "6x4", "6x5"],
  ["6x3", "6x4", "6x5", "6x6"],
  ["6x4", "6x5", "6x6", "6x7"],
  ["1x1", "2x1", "3x1", "4x1"],
  ["2x1", "3x1", "4x1", "5x1"],
  ["3x1", "4x1", "5x1", "6x1"],
  ["1x2", "2x2", "3x2", "4x2"],
  ["2x2", "3x2", "4x2", "5x2"],
  ["3x2", "4x2", "5x2", "6x2"],
  ["1x3", "2x3", "3x3", "4x3"],
  ["2x3", "3x3", "4x3", "5x3"],
  ["3x3", "4x3", "5x3", "6x3"],
  ["1x4", "2x4", "3x4", "4x4"],
  ["2x4", "3x4", "4x4", "5x4"],
  ["3x4", "4x4", "5x4", "6x4"],
  ["1x5", "2x5", "3x5", "4x5"],
  ["2x5", "3x5", "4x5", "5x5"],
  ["3x5", "4x5", "5x5", "6x5"],
  ["1x6", "2x6", "3x6", "4x6"],
  ["2x6", "3x6", "4x6", "5x6"],
  ["3x6", "4x6", "5x6", "6x6"],
  ["1x7", "2x7", "3x7", "4x7"],
  ["2x7", "3x7", "4x7", "5x7"],
  ["3x7", "4x7", "5x7", "6x7"],
  ["4x1", "3x2", "2x3", "1x4"],
  ["5x1", "4x2", "3x3", "2x4"],
  ["6x1", "5x2", "4x3", "3x4"],
  ["4x2", "3x3", "2x4", "1x5"],
  ["5x2", "4x3", "3x4", "2x5"],
  ["6x2", "5x3", "4x4", "3x5"],
  ["4x3", "3x4", "2x5", "1x6"],
  ["5x3", "4x4", "3x5", "2x6"],
  ["6x3", "5x4", "4x5", "3x6"],
  ["4x4", "3x5", "2x6", "1x7"],
  ["5x4", "4x5", "3x6", "2x7"],
  ["6x4", "5x5", "4x6", "3x7"],
  ["1x1", "2x2", "3x3", "4x4"],
  ["2x1", "3x2", "4x3", "5x4"],
  ["3x1", "4x2", "5x3", "6x4"],
  ["1x2", "2x3", "3x4", "4x5"],
  ["2x2", "3x3", "4x4", "5x5"],
  ["3x2", "4x3", "5x4", "6x5"],
  ["1x3", "2x4", "3x5", "4x6"],
  ["2x3", "3x4", "4x5", "5x6"],
  ["3x3", "4x4", "5x5", "6x6"],
  ["1x4", "2x5", "3x6", "4x7"],
  ["2x4", "3x5", "4x6", "5x7"],
  ["3x4", "4x5", "5x6", "6x7"],
]

for (let i = 0; i < winningCombinations.length; i++) {
  var one = document.getElementById(winningCombinations[i][0]);
  var two = document.getElementById(winningCombinations[i][1]);
  var three = document.getElementById(winningCombinations[i][2]);
  var four = document.getElementById(winningCombinations[i][3]);

  if (one.hasChildNodes() && 
      two.hasChildNodes() &&
      three.hasChildNodes() && 
      four.hasChildNodes()) {

    if (one.firstChild.classList.contains("black") &&
        two.firstChild.classList.contains("black") &&
        three.firstChild.classList.contains("black") &&
        four.firstChild.classList.contains("black")) {
          one.style.backgroundColor = "yellow";
          two.style.backgroundColor = "yellow";
          three.style.backgroundColor = "yellow";
          four.style.backgroundColor = "yellow";
          alert("The color BLACK has won!!!") ? "" : setTimeout("location.reload(true)", 5000);


    } else if (!one.firstChild.classList.contains("black") &&
               !two.firstChild.classList.contains("black") &&
               !three.firstChild.classList.contains("black") &&
               !four.firstChild.classList.contains("black")) {
                one.style.backgroundColor = "yellow";
                two.style.backgroundColor = "yellow";
                three.style.backgroundColor = "yellow";
                four.style.backgroundColor = "yellow";
                alert("The color RED has won!!!") ? "" : setTimeout("location.reload(true)", 1500);
    } 
    else if (
      document.getElementById("1x1").hasChildNodes() &&
      document.getElementById("1x2").hasChildNodes() &&
      document.getElementById("1x3").hasChildNodes() &&
      document.getElementById("1x4").hasChildNodes() &&
      document.getElementById("1x5").hasChildNodes() &&
      document.getElementById("1x6").hasChildNodes() &&
      document.getElementById("1x7").hasChildNodes()
    ) {
      table.style.backgroundColor = "yellow";
      alert("THIS IS A TIE! \n Please close the game") ? "" : setTimeout("location.reload(true)", 500);
    }
  }
}
}


